import React, { Component } from 'react'
import {Link} from 'react-router-dom'

class SubPosts extends Component {

    render() {
        const {
            id,
            image = 'images/wrapper.jpg',
            section,
            title,
            mainText,
        } = this.props;

        return (
            <div className="card">
                <img src={image} alt={title} className="card-img-top"/>
                <div className="card-body">
                    <p className="card-text">
                        <small className="text-muted text-uppercase">{section}</small>
                    </p>
                    <h2 className="card-title">
                        <Link to={{pathname: '/post/'+id}}>
                            {title}
                        </Link>
                    </h2>
                    <p className="card-text text-overflow main-text">{mainText}</p>
                </div>
            </div>
        )
    }
}

export default SubPosts