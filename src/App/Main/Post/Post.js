import React from 'react'
import posts from '../MainPost/SubPosts/posts'

const Post = (match) => {

    const postId = match.match.params.id;
    const post = posts.find(item => item.id === +postId);

    if (typeof post.image === 'undefined') {
        post.image = '/images/wrapper.jpg';
    }

    return (
        <div>
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-12">
                        <div className="card">
                            <img src={post.image} className="card-img-top" alt="..."/>
                            <div className="row d-flex justify-content-center">
                                <div className="col-md-10">
                                    <div className="card-body first-post">
                                        <p className="card-text">
                                            <small className="text-muted text-uppercase">{post.section}</small>
                                        </p>
                                        <h2 className="card-title">{post.title}</h2>
                                        <p className="card-text">{post.mainText}</p>
                                            <blockquote className="blockquote font-italic">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aut
                                                    cum error in magnam neque obcaecati odio quibusdam reprehenderit
                                                    similique, vero voluptatibus? Ab accusamus ad at, eveniet iste
                                                    obcaecati quidem!</p>
                                            </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="another-posts-field">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-10">
                            <small className="text-muted text-uppercase">See also</small>
                        </div>
                    </div>
                </div>
            </div>
            <div className="comments-field">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-10">
                            <small className="text-muted text-uppercase">Comments</small>
                            <ul className="list-unstyled">
                                <li className="media">
                                    <img src="" className="mr-3" alt="..."/>
                                        <div className="media-body">
                                            <h5 className="mt-0 mb-1">List-based media object</h5>
                                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                                            ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus
                                            viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec
                                            lacinia congue felis in faucibus.
                                        </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Post