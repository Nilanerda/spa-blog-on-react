import React from 'react'
import {Route, Switch} from 'react-router-dom'
import './main.css'

import MainPost from './MainPost/MainPost'
import Post from './Post/Post'

const Main = () =>
    <div>
        <div className="container">
            <div className="row d-flex justify-content-center">
                <div className="col-12">
                    <Switch>
                        <Route path="/post/:id" component={Post}/>
                        <Route exact path="/" component={MainPost}/>
                    </Switch>
                </div>
            </div>
        </div>
    </div>


export default Main