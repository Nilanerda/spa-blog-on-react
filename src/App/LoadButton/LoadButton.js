import React from 'react'
import './load-button.css'

const LoadButton = () =>
    <div>
        <div className="container-fluid">
            <div className="row justify-content-center btn-row">
                <button className="btn btn-outline-secondary load-more-btn px-5 py-3 effect-shine">Load more</button>
            </div>
        </div>
    </div>

export default LoadButton