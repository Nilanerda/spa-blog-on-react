import React from 'react'

const Licence = () =>
    <div>
        <a href="" className="footer-link effect-shine">Terms and conditions</a>
        <a href="" className="footer-link effect-shine">Privacy</a>
    </div>

export default Licence