import React, { Component } from 'react'
import {Link} from 'react-router-dom'

class FirstPost extends Component {
    render() {
        const {
            id,
            image = 'images/wrapper.jpg',
            section,
            title,
            mainText,
        } = this.props;

        return (
            <div key={id}>
                {/*<img src={image} className="card-img-top" alt={title}/>*/}
                <div className="card-body first-post">
                    <p className="card-text">
                        <small className="text-muted text-uppercase">{section}</small>
                    </p>
                    <h2 className="card-title">
                        <Link to={{pathname: '/post/'+id}}>
                            {title}
                        </Link>
                    </h2>
                    <p className="card-text">{mainText}</p>
                </div>
            </div>

        )
    }

}

export default FirstPost