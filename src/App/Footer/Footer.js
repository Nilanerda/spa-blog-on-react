import React from 'react'
import './Licence/licence.css'
import './SocialLink/social-link.css'

import Licence from "./Licence/Licence"
import SocialLink from './SocialLink/SocialLink'

const Footer = () =>
    <div>
        <footer className="footer py-5">
            <div className="container">
                <div className="row">
                    <div className="col-auto">
                        <Licence/>
                    </div>
                    <div className="col-auto ml-auto">
                        <SocialLink/>
                    </div>
                </div>
            </div>
        </footer>
    </div>

export default Footer