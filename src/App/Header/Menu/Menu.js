import React from 'react'

const Menu = () =>
    <div className="collapse navbar-collapse">
        <ul className="navbar-nav ml-auto main-nav d-inline-flex justify-content-between">
            <li className="nav-item">
                <a className="nav-link effect-shine" href="#">Lifestyle</a>
            </li>
            <li className="nav-item">
                <a className="nav-link effect-shine" href="#">Photodiary</a>
            </li>
            <li className="nav-item">
                <a className="nav-link effect-shine" href="#">Music</a>
            </li>
            <li className="nav-item">
                <a className="nav-link effect-shine" href="#">Travel</a>
            </li>
        </ul>
    </div>

export default Menu