import React from 'react'

const Logo = () =>
    <div>
        <a href="/" className="navbar-brand logo-name">minimø</a>
    </div>

export default Logo