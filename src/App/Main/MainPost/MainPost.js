import React from 'react'
import SubPosts from './SubPosts/SubPosts'
import FirstPost from './SubPosts/FirstPost'
import posts from './SubPosts/posts'

const MainPost = () => {
    var post = posts.shift();
    if (typeof post.image === 'undefined'){
        post.image = '/images/wrapper.jpg';
    }
    return (
        <div>
            <div className="card">
                <img src={post.image} className="card-img-top" alt={post.title}/>
                <div className="row d-flex justify-content-center">
                    <div className="col-md-10" key={post.id}>
                        <FirstPost
                            id={post.id}
                            section={post.section}
                            title={post.title}
                            mainText={post.mainText}
                        />
                        <div className="row d-flex justify-content-between">
                            {
                                posts.map(({id, image, section, title, mainText,}) => (
                                    <div className="col-md-5 col-xl-5-5" key={id}>
                                        <SubPosts
                                            id={id}
                                            image={image}
                                            section={section}
                                            title={title}
                                            mainText={mainText}
                                        />
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default MainPost