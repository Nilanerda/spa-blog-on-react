import React from 'react'
import {Route} from 'react-router-dom'

import './../common/style/base.css'

import Header from './Header/Header'
import Main from './Main/Main'
import LoadButton from './LoadButton/LoadButton'
import Footer from './Footer/Footer'


import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import MainPost from "./Main/MainPost/MainPost";
library.add(fab)

const App = () =>
    <div>
        <Header />
        <Main />
        <Route exact path="/" component={LoadButton}/>
        <Footer />
    </div>


export default App
