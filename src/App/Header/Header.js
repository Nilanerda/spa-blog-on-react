import React from 'react'

import './header.css'
import './Logo/logo.css'
import './Menu/menu.css'


import Logo from './Logo/Logo'
import Menu from './Menu/Menu'

const Header = () =>
    <div>
        <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container">
                <Logo/>
                <Menu/>
            </div>
        </nav>
    </div>

export default Header