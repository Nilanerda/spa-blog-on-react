import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const SocialLink = () =>
    <div>
        <span className="follow">Follow</span>
        <a href="" className="footer-link">
            <FontAwesomeIcon icon={['fab', 'facebook-f']} />
        </a>
        <a href="" className="footer-link">
            <FontAwesomeIcon icon={['fab', 'twitter']} />
        </a>
        <a href="" className="footer-link">
            <FontAwesomeIcon icon={['fab', 'instagram']} />
        </a>
    </div>


export default SocialLink